package com.appstudioz.clgui.utils;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


/**
 * Created by kumar on 11/4/2017.
 */

public class FragmentTransections {


    public static void replaceFragmnet(Context context, Fragment fragment, String tag, int layout, Boolean isAddFrag) {
        FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.anim.slide_in_right,0);

        ft.replace(layout, fragment, tag);
        ft.addToBackStack(tag);

        if (!isAddFrag) {
            if (((AppCompatActivity) context).getSupportFragmentManager().getBackStackEntryCount() > 1) {
                for (int i = ((AppCompatActivity) context).getSupportFragmentManager().getBackStackEntryCount(); i > 1; i--) {
                    ((AppCompatActivity) context).getSupportFragmentManager().popBackStack();
                }
            }
        }
        Log.e("FragmentCount", "" + ((AppCompatActivity) context).getSupportFragmentManager().getBackStackEntryCount() + " Tag " + tag);


        ft.commit();

    }


    public static Fragment currentFragment(FragmentManager fragmentManager, int layout_id)
    {
        return fragmentManager.findFragmentById(layout_id);

    }

}
