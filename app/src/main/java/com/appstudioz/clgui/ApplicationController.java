package com.appstudioz.clgui;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationController extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {
    private static ApplicationController mApplicationInstance;
    private boolean mIsNetworkConnected;
    private Activity activity;
    private Typeface latoBold;
    private int randomNumberForAd = 3;

    public static ApplicationController getApplicationInstance() {
        if (mApplicationInstance == null)
            mApplicationInstance = new ApplicationController();
        return mApplicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf").setFontAttrId(R.attr.fontPath).build());//Roboto Condensed
        registerActivityLifecycleCallbacks(this);
        mApplicationInstance = this;
    }


    public boolean isNetworkConnected() {
        return mIsNetworkConnected;
    }

    public void setIsNetworkConnected(boolean mIsNetworkConnected) {
        this.mIsNetworkConnected = mIsNetworkConnected;
    }

    public Typeface getLatoBold() {
        if (latoBold == null)
            latoBold = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");

        return latoBold;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public int getRandomNumberForAd() {
        return randomNumberForAd;
    }

    public void setRandomNumberForAd(int randomNumberForAd) {
        this.randomNumberForAd = randomNumberForAd;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(getApplicationContext());
    }

}