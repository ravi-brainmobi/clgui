package com.appstudioz.clgui;

public interface PlayerClicked {
        void onPlayerItemClicked(int position);
    }