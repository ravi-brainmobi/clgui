package com.appstudioz.clgui.fragment;

import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.base.BaseFragment;

import static com.appstudioz.clgui.constants.FragmentsTags.POLLS;

public class PollsFragment extends BaseFragment {


    @Override
    protected void initUi() {

        ((HomeActivity) getActivity()).setSelectedViews(POLLS);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_polling;
    }

    @Override
    public void onClick(View v) {

    }
}
