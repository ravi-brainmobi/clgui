package com.appstudioz.clgui.fragment;

import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.base.BaseFragment;

import static com.appstudioz.clgui.constants.FragmentsTags.HOME;

public class HomeFragment extends BaseFragment {
    @Override
    protected void initUi() {

        ((HomeActivity)getActivity()).setSelectedViews(HOME);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_home;
    }

    @Override
    public void onClick(View v) {

    }
}
