package com.appstudioz.clgui.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.adapter.MatchesPagerAdapter;
import com.appstudioz.clgui.base.BaseFragment;
import com.appstudioz.clgui.databinding.FragmentUpcomingMatchesBinding;

import java.util.ArrayList;

import static com.appstudioz.clgui.constants.FragmentsTags.HOME;
import static com.appstudioz.clgui.constants.FragmentsTags.UPCOMING_MATCHES;

public class UpcomingMatchesFragment extends BaseFragment {

    private FragmentUpcomingMatchesBinding mRecentMatchesBinding;
    private ArrayList<Fragment> matchesFragmentsArrayList = new ArrayList<>();

    @Override
    protected void initUi() {
        mRecentMatchesBinding = (FragmentUpcomingMatchesBinding) viewDataBinding;
        ((HomeActivity)getActivity()).setSelectedViews(UPCOMING_MATCHES);
        prepareFragment();
        setupPager();
    }

    private void prepareFragment() {
        matchesFragmentsArrayList.add(new MatchListFragment());
        matchesFragmentsArrayList.add(new MatchListFragment());
        matchesFragmentsArrayList.add(new MatchListFragment());
    }

    private void setupPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        MatchesPagerAdapter myPagerAdapter = new MatchesPagerAdapter(getActivity().getSupportFragmentManager(), matchesFragmentsArrayList);
        viewPager.setAdapter(myPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_upcoming_matches;
    }

    @Override
    public void onClick(View view) {

    }
}
