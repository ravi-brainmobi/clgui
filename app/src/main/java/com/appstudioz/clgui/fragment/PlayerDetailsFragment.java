package com.appstudioz.clgui.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.adapter.PlayerDetailsPagerAdapter;
import com.appstudioz.clgui.base.BaseFragment;
import com.appstudioz.clgui.databinding.FragmentPlayerDetailsBinding;
import com.appstudioz.clgui.fragment.playerDetails.PlayerDetailBattingFragment;
import com.appstudioz.clgui.fragment.playerDetails.PlayerDetailBowlingFragment;
import com.appstudioz.clgui.fragment.playerDetails.PlayerDetailInfoFragment;

import java.util.ArrayList;

import static com.appstudioz.clgui.constants.FragmentsTags.MORE;

public class PlayerDetailsFragment extends BaseFragment {

    private FragmentPlayerDetailsBinding mPlayerDetailsBinding;
    private ArrayList<Fragment> playerInfoFragmentsArrayList = new ArrayList<>();

    @Override
    protected void initUi() {
        mPlayerDetailsBinding = (FragmentPlayerDetailsBinding) viewDataBinding;
        ((HomeActivity) getActivity()).setSelectedViews(MORE);
        prepareFragment();
        setupPager();
    }

    private void prepareFragment() {
        playerInfoFragmentsArrayList.add(new PlayerDetailInfoFragment());
        playerInfoFragmentsArrayList.add(new PlayerDetailBattingFragment());
        playerInfoFragmentsArrayList.add(new PlayerDetailBowlingFragment());
    }

    private void setupPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        PlayerDetailsPagerAdapter myPagerAdapter = new PlayerDetailsPagerAdapter(getActivity().getSupportFragmentManager(), playerInfoFragmentsArrayList);
        viewPager.setAdapter(myPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_player_details;
    }

    @Override
    public void onClick(View view) {

    }
}
