package com.appstudioz.clgui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.adapter.MatchListAdapter;
import com.appstudioz.clgui.base.BaseFragment;
import com.appstudioz.clgui.databinding.FragmentMatchListBinding;

public class MatchListFragment extends BaseFragment {

    private FragmentMatchListBinding matchListBinding;

    @Override
    protected void initUi() {
        matchListBinding = (FragmentMatchListBinding) viewDataBinding;
        setMatchList();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setMatchList() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        MatchListAdapter matchListAdapter = new MatchListAdapter();
        matchListBinding.rvMatchList.setLayoutManager(layoutManager);
        matchListBinding.rvMatchList.setAdapter(matchListAdapter);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_match_list;
    }

    @Override
    public void onClick(View v) {

    }
}
