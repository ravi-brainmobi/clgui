package com.appstudioz.clgui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.appstudioz.clgui.PlayerClicked;
import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.adapter.BrowsePlayerListAdapter;
import com.appstudioz.clgui.base.BaseFragment;
import com.appstudioz.clgui.databinding.FragmentBrowsePlayerBinding;
import com.appstudioz.clgui.utils.FragmentTransections;

import java.util.ArrayList;
import java.util.Collections;

import static android.content.ContentValues.TAG;
import static com.appstudioz.clgui.activities.HomeActivity.fragmentContainerLayout;
import static com.appstudioz.clgui.constants.FragmentsTags.MORE;
import static com.appstudioz.clgui.constants.FragmentsTags.PLAYER_DETAIL_INFO;

public class BrowsePlayerFragment extends BaseFragment implements PlayerClicked {

    private FragmentBrowsePlayerBinding mBrowsePlayerBinding;
    private ArrayList<String> mPlayerList = new ArrayList<>();

    @Override
    protected void initUi() {
        mBrowsePlayerBinding = (FragmentBrowsePlayerBinding) viewDataBinding;
        ((HomeActivity) getActivity()).setSelectedViews(MORE);
        preparePlayerList();
        setPlayerList();
    }

    private void setPlayerList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        Collections.sort(mPlayerList);
        BrowsePlayerListAdapter playerListAdapter = new BrowsePlayerListAdapter(mPlayerList, this);
        mBrowsePlayerBinding.rvPlayerList.setLayoutManager(layoutManager);
        mBrowsePlayerBinding.rvPlayerList.setIndexBarTextColor(R.color.colorTextPurple);
        mBrowsePlayerBinding.rvPlayerList.setIndexBarTransparentValue((float) 0);
        mBrowsePlayerBinding.rvPlayerList.setIndexbarMargin(10);
        mBrowsePlayerBinding.rvPlayerList.setAdapter(playerListAdapter);
    }

    private void preparePlayerList() {
        mPlayerList.add("A player");
        mPlayerList.add("F player");
        mPlayerList.add("H player");
        mPlayerList.add("L player");
        mPlayerList.add("R player");
        mPlayerList.add("S player");
        mPlayerList.add("T player");
        mPlayerList.add("U player");
        mPlayerList.add("V player");
        mPlayerList.add("W player");
        mPlayerList.add("X player");
        mPlayerList.add("Y player");
        mPlayerList.add("Ad player");
        mPlayerList.add("Fa player");
        mPlayerList.add("Hh player");
        mPlayerList.add("Ls player");
        mPlayerList.add("Rf player");
        mPlayerList.add("Ss player");
        mPlayerList.add("Ts player");
        mPlayerList.add("Us player");
        mPlayerList.add("Vh player");
        mPlayerList.add("Wa player");
        mPlayerList.add("Xk player");
        mPlayerList.add("Yw player");
        mPlayerList.add("Adw player");
        mPlayerList.add("Faa player");
        mPlayerList.add("Hha player");
        mPlayerList.add("Lsj player");
        mPlayerList.add("Rfw player");
        mPlayerList.add("Ssh player");
        mPlayerList.add("Tsw player");
        mPlayerList.add("Ush player");
        mPlayerList.add("Vhw player");
        mPlayerList.add("Waw player");
        mPlayerList.add("Xkg player");
        mPlayerList.add("Ywe player");
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_browse_player;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPlayerItemClicked(int position) {
        Log.d(TAG, "onPlayerItemClicked: " + position);
        FragmentTransections.replaceFragmnet(
                getActivity(),
                new PlayerDetailsFragment(),
                PLAYER_DETAIL_INFO,
                fragmentContainerLayout,
                true);
    }
}
