package com.appstudioz.clgui.fragment;

import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.activities.HomeActivity;
import com.appstudioz.clgui.base.BaseFragment;
import com.appstudioz.clgui.databinding.FragmentMoreBinding;
import com.appstudioz.clgui.utils.FragmentTransections;

import static com.appstudioz.clgui.activities.HomeActivity.fragmentContainerLayout;
import static com.appstudioz.clgui.constants.FragmentsTags.BROWSE_PLAYER_LIST;
import static com.appstudioz.clgui.constants.FragmentsTags.MORE;
import static com.appstudioz.clgui.constants.FragmentsTags.RECENT_MATCHES;

public class MoreFragment extends BaseFragment {

    private FragmentMoreBinding fragmentMoreBinding;

    @Override
    protected void initUi() {
        fragmentMoreBinding = (FragmentMoreBinding) viewDataBinding;
        ((HomeActivity) getActivity()).setSelectedViews(MORE);
        setClickListeners();
    }

    private void setClickListeners() {
        fragmentMoreBinding.llBrowseSeriesBtn.setOnClickListener(this);
        fragmentMoreBinding.llBrowsePlayerBtn.setOnClickListener(this);
        fragmentMoreBinding.llBrowseTeamBtn.setOnClickListener(this);
        fragmentMoreBinding.llRankingBtn.setOnClickListener(this);
        fragmentMoreBinding.llNewsBtn.setOnClickListener(this);
        fragmentMoreBinding.llRateUsBtn.setOnClickListener(this);
        fragmentMoreBinding.llFeedbackBtn.setOnClickListener(this);
        fragmentMoreBinding.llSettingBtn.setOnClickListener(this);
        fragmentMoreBinding.llDisclaimerBtn.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_more;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_browse_series_btn:

                break;
            case R.id.ll_browse_player_btn:
                FragmentTransections.replaceFragmnet(
                        getActivity(),
                        new BrowsePlayerFragment(),
                        BROWSE_PLAYER_LIST,
                        fragmentContainerLayout,
                        true);
                break;
            case R.id.ll_browse_team_btn:

                break;
            case R.id.ll_ranking_btn:

                break;
            case R.id.ll_news_btn:

                break;
            case R.id.ll_rate_us_btn:

                break;
            case R.id.ll_feedback_btn:

                break;
            case R.id.ll_setting_btn:

                break;
            case R.id.ll_disclaimer_btn:

                break;
        }
    }
}
