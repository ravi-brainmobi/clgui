package com.appstudioz.clgui.fragment.playerDetails;

import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.base.BaseFragment;

public class PlayerDetailInfoFragment extends BaseFragment {
    @Override
    protected void initUi() {

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_player_details_info;
    }

    @Override
    public void onClick(View v) {

    }
}
