package com.appstudioz.clgui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.databinding.RvItemMatchListBinding;

public class MatchListAdapter extends RecyclerView.Adapter<MatchListAdapter.MatchListViewHolder>{


    @NonNull
    @Override
    public MatchListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvItemMatchListBinding matchListBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.rv_item_match_list,
                        parent,
                        false);
        return new MatchListViewHolder(matchListBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull MatchListViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class MatchListViewHolder extends RecyclerView.ViewHolder {
        public MatchListViewHolder(View itemView) {
            super(itemView);
        }
    }
}
