package com.appstudioz.clgui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.appstudioz.clgui.PlayerClicked;
import com.appstudioz.clgui.R;
import com.appstudioz.clgui.databinding.RvItemPlayerListBinding;
import java.util.ArrayList;
import java.util.List;

public class BrowsePlayerListAdapter
        extends RecyclerView.Adapter<BrowsePlayerListAdapter.BrowsePlayerListViewHolder>
        implements SectionIndexer {

    private ArrayList<String> mPlayerList;
    private ArrayList<Integer> mSectionPositions;
    private PlayerClicked playerClickedListener;

    public BrowsePlayerListAdapter(ArrayList<String> mPlayerList, PlayerClicked playerClickedListener) {
        this.mPlayerList = mPlayerList;
        this.playerClickedListener = playerClickedListener;
    }

    @NonNull
    @Override
    public BrowsePlayerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvItemPlayerListBinding mPlayerListBinding = DataBindingUtil
                .inflate(
                        LayoutInflater.from(parent.getContext()),
                        R.layout.rv_item_player_list,
                        parent,
                        false
                );
        return new BrowsePlayerListViewHolder(mPlayerListBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BrowsePlayerListViewHolder holder, final int position) {
        holder.mPlayerName.setText(mPlayerList.get(position));
        holder.mPlayerListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerClickedListener.onPlayerItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPlayerList.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = mPlayerList.size(); i < size; i++) {
            String section = String.valueOf(mPlayerList.get(i).charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    class BrowsePlayerListViewHolder extends RecyclerView.ViewHolder {

        TextView mPlayerName;
        LinearLayout mPlayerListItem;

        public BrowsePlayerListViewHolder(RvItemPlayerListBinding itemView) {
            super(itemView.getRoot());
            mPlayerName = itemView.tvPlayerName;
            mPlayerListItem = itemView.llPlayerItem;
        }
    }
}
