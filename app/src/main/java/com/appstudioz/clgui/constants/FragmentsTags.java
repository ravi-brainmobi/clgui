package com.appstudioz.clgui.constants;

public interface FragmentsTags {

    String HOME = "home";
    String RECENT_MATCHES = "recent_matches";
    String UPCOMING_MATCHES = "upcoming_matches";
    String POLLS = "polls";
    String MORE = "more";
    String BROWSE_PLAYER_LIST = "browse_player_list";
    String PLAYER_DETAIL_INFO = "player_detail_info";

}
