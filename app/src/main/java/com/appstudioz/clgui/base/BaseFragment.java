package com.appstudioz.clgui.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;


public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    private View view;
    protected Context context;
    protected ViewDataBinding viewDataBinding;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);
        if (viewDataBinding != null)
            return viewDataBinding.getRoot();
        else return inflater.inflate(getLayoutById(), container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUi();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    protected View findViewById(int resId) {

        return view.findViewById(resId);
    }

    /**
     * Initilize Ui parameters here
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     *
     * @return Layout res id
     */
    protected abstract int getLayoutById();


}
