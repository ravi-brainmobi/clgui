package com.appstudioz.clgui.base;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    protected ViewDataBinding viewDataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutById());

      /*  mToolBar = (Toolbar) findViewById(R.id.toolbar_main_screen);
        titleTv = (TextView) findViewById(R.id.textView_toolbar_main_screen);*/
        initView();
    }

    public void sendLocalBroadcastMessage(String localBroadcastMessage) {
        LocalBroadcastManager
                .getInstance(this)
                .sendBroadcast(new Intent(localBroadcastMessage));
        finish();
    }

    public void hideSoftInputKeyboard(View view) {
        if (view != null) {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    protected abstract void initView();

    protected abstract int getLayoutById();


}
