package com.appstudioz.clgui.activities;

import android.support.v4.app.Fragment;
import android.view.View;

import com.appstudioz.clgui.R;
import com.appstudioz.clgui.base.BaseActivity;
import com.appstudioz.clgui.databinding.ActivityHomeBinding;
import com.appstudioz.clgui.fragment.HomeFragment;
import com.appstudioz.clgui.fragment.MoreFragment;
import com.appstudioz.clgui.fragment.PollsFragment;
import com.appstudioz.clgui.fragment.RecentMatchesFragment;
import com.appstudioz.clgui.fragment.UpcomingMatchesFragment;
import com.appstudioz.clgui.utils.FragmentTransections;

import static com.appstudioz.clgui.constants.FragmentsTags.HOME;
import static com.appstudioz.clgui.constants.FragmentsTags.MORE;
import static com.appstudioz.clgui.constants.FragmentsTags.POLLS;
import static com.appstudioz.clgui.constants.FragmentsTags.RECENT_MATCHES;
import static com.appstudioz.clgui.constants.FragmentsTags.UPCOMING_MATCHES;

public class HomeActivity extends BaseActivity {

    private ActivityHomeBinding mHomeBinding;
    public static int fragmentContainerLayout = R.id.fragment_container;

    @Override
    protected void initView() {
        mHomeBinding = (ActivityHomeBinding) viewDataBinding;
        mHomeBinding.includeBottomAppBar.llHome.setOnClickListener(this);
        mHomeBinding.includeBottomAppBar.llRecentMatches.setOnClickListener(this);
        mHomeBinding.includeBottomAppBar.llUpcoming.setOnClickListener(this);
        mHomeBinding.includeBottomAppBar.llPolls.setOnClickListener(this);
        mHomeBinding.includeBottomAppBar.llMore.setOnClickListener(this);
        onClick(mHomeBinding.includeBottomAppBar.llHome);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_home;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llHome:
                FragmentTransections.replaceFragmnet(
                        this,
                        new HomeFragment(),
                        HOME,
                        fragmentContainerLayout,
                        true);
                setSelectedViews(HOME);
                break;
            case R.id.llRecentMatches:
                FragmentTransections.replaceFragmnet(
                        this,
                        new RecentMatchesFragment(),
                        RECENT_MATCHES,
                        fragmentContainerLayout,
                        false);
                setSelectedViews(RECENT_MATCHES);
                break;
            case R.id.llUpcoming:
                FragmentTransections.replaceFragmnet(
                        this, new
                                UpcomingMatchesFragment(),
                        UPCOMING_MATCHES,
                        fragmentContainerLayout,
                        false);
                setSelectedViews(UPCOMING_MATCHES);
                break;
            case R.id.llPolls:
                FragmentTransections.replaceFragmnet(
                        this,
                        new PollsFragment(),
                        POLLS,
                        fragmentContainerLayout,
                        false);
                setSelectedViews(POLLS);
                break;
            case R.id.llMore:
                FragmentTransections.replaceFragmnet(
                        this,
                        new MoreFragment(),
                        MORE,
                        fragmentContainerLayout,
                        false);
                setSelectedViews(MORE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        /*Fragment fragment = FragmentTransections.currentFragment(getSupportFragmentManager(), fragmentContainerLayout);
        if (fragment instanceof HomeFragment) {
            finish();
        } else {
            FragmentTransections.replaceFragmnet(
                    this,
                    new HomeFragment(),
                    HOME, fragmentContainerLayout,
                    false);

        }*/
        if (getSupportFragmentManager().getBackStackEntryCount() >= 2) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public void replaceFragment(String tag) {

    }

    public void setSelectedViews(String tag) {
        resetBottomAppBarViews();
        switch (tag) {
            case HOME:
//                mHomeBinding.includeBottomAppBar.tvHome.setTextColor(getResources().getColor(R.color.colorTextActiveState));
//                mHomeBinding.includeBottomAppBar.ivHome.setImageResource(R.drawable.home_active);
                mHomeBinding.includeBottomAppBar.viewHome.setVisibility(View.VISIBLE);
                break;
            case RECENT_MATCHES:
//                mHomeBinding.includeBottomAppBar.tvRecentMatches.setTextColor(getResources().getColor(R.color.colorTextActiveState));
//                mHomeBinding.includeBottomAppBar.ivRecentMatches.setImageResource(R.drawable.matches_active);
                mHomeBinding.includeBottomAppBar.viewRecentMatches.setVisibility(View.VISIBLE);
                break;
            case UPCOMING_MATCHES:
//                mHomeBinding.includeBottomAppBar.tvUpcoming.setTextColor(getResources().getColor(R.color.colorTextActiveState));
//                mHomeBinding.includeBottomAppBar.ivUpcoming.setImageResource(R.drawable.upcoming_active);
                mHomeBinding.includeBottomAppBar.viewUpcoming.setVisibility(View.VISIBLE);
                break;
            case POLLS:
//                mHomeBinding.includeBottomAppBar.tvPolls.setTextColor(getResources().getColor(R.color.colorTextActiveState));
//                mHomeBinding.includeBottomAppBar.ivPolls.setImageResource(R.drawable.poles_active);
                mHomeBinding.includeBottomAppBar.viewPolls.setVisibility(View.VISIBLE);
                break;
            case MORE:
//                mHomeBinding.includeBottomAppBar.tvMore.setTextColor(getResources().getColor(R.color.colorTextActiveState));
//                mHomeBinding.includeBottomAppBar.ivMore.setImageResource(R.drawable.more_active);
                mHomeBinding.includeBottomAppBar.viewMore.setVisibility(View.VISIBLE);
                break;

        }
    }

    public void resetBottomAppBarViews() {

       /* //reset TextView
        mHomeBinding.includeBottomAppBar.tvHome.setTextColor(getResources().getColor(R.color.colorTextInactiveState));
        mHomeBinding.includeBottomAppBar.tvRecentMatches.setTextColor(getResources().getColor(R.color.colorTextInactiveState));
        mHomeBinding.includeBottomAppBar.tvUpcoming.setTextColor(getResources().getColor(R.color.colorTextInactiveState));
        mHomeBinding.includeBottomAppBar.tvPolls.setTextColor(getResources().getColor(R.color.colorTextInactiveState));
        mHomeBinding.includeBottomAppBar.tvMore.setTextColor(getResources().getColor(R.color.colorTextInactiveState));

        //reset ImageViews
        mHomeBinding.includeBottomAppBar.ivHome.setImageResource(R.drawable.home_inactive);
        mHomeBinding.includeBottomAppBar.ivRecentMatches.setImageResource(R.drawable.matches_inactive);
        mHomeBinding.includeBottomAppBar.ivUpcoming.setImageResource(R.drawable.upcoming_inactive);
        mHomeBinding.includeBottomAppBar.ivPolls.setImageResource(R.drawable.poles_inactive);
        mHomeBinding.includeBottomAppBar.ivMore.setImageResource(R.drawable.more_inactive);
*/
        //resetViews
        mHomeBinding.includeBottomAppBar.viewHome.setVisibility(View.INVISIBLE);
        mHomeBinding.includeBottomAppBar.viewRecentMatches.setVisibility(View.INVISIBLE);
        mHomeBinding.includeBottomAppBar.viewUpcoming.setVisibility(View.INVISIBLE);
        mHomeBinding.includeBottomAppBar.viewPolls.setVisibility(View.INVISIBLE);
        mHomeBinding.includeBottomAppBar.viewMore.setVisibility(View.INVISIBLE);
    }
}
